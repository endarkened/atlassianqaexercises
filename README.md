To run tests you'll first need to add 'gradle.properties' file to the root of this project containing
~~~~
	systemProp.test.username = <your username>
	systemProp.test.password = <your password>
~~~~
and run 'gradle test' command from you console. Simple as that! Currently the solution supports only Chrome browser but that could be easily extended.

Important notice: the code was written based on the literal understanding of the task, and does not fully reflect my approach to test automation. Given more time and more concrete aim, the design would be a lot more different. And lot more efficient.