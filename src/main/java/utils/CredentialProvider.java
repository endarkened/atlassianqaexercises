package utils;

public class CredentialProvider {

	private String username = "";
	private String password = "";

	public CredentialProvider() throws Exception {
		// need adding file, db, etc.
		loadFromSystemProperties();
	}

	private void loadFromSystemProperties() throws Exception {
		if (System.getProperty("test.username") == null
				|| System.getProperty("test.username").isEmpty()) {
			throw new Exception(
					"System property \"test.username\" is not specified or empty");
		}
		if (System.getProperty("test.password") == null
				|| System.getProperty("test.password").isEmpty()) {
			throw new Exception(
					"System property \"test.password\" is not specified or empty");
		}
		this.username = System.getProperty("test.username");
		this.password = System.getProperty("test.password");
	}

	public String getUsername() {
		return this.username;
	}

	public String getPassword() {
		return this.password;
	}
}
