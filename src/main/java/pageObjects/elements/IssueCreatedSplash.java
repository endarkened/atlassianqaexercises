package pageObjects.elements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import pageObjects.AbstractPageObject;
import pageObjects.pages.IssuePage;

public class IssueCreatedSplash extends AbstractPageObject {

	private final static By splashRegion = By.className("aui-message-success");
	private final static By issueCreatedLink = By.className("issue-link");

	private WebElement createdLink;

	public IssueCreatedSplash(WebDriver driver) {
		super(driver);

		this.createdLink = this.driver.findElement(splashRegion).findElement(
				issueCreatedLink);
	}

	public String getIssueName() {
		return createdLink.getAttribute("data-issue-key");
	}

	public IssuePage navigateToIssue() {
		this.driver.navigate().to(this.createdLink.getAttribute("href"));
		return new IssuePage(this.driver);
	}
}
