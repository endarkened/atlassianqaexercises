package pageObjects.elements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import pageObjects.AbstractPageObject;

public class CreateIssueSplash extends AbstractPageObject {
	
	private static final By splashLocator = By.id("create-issue-dialog");
	private static final By summaryFieldLocator = By.id("summary");
	private static final By submitButtonLocator = By.id("create-issue-submit");

	
	public CreateIssueSplash(WebDriver driver) {
		super(driver);
		this.driver.findElement(splashLocator);
	}

	public void enterSummary(String summary) {
		this.driver.findElement(summaryFieldLocator).sendKeys(summary);
	}

	public IssueCreatedSplash submit() {
		this.driver.findElement(submitButtonLocator).click();
		return new IssueCreatedSplash(this.driver);
	}

}
