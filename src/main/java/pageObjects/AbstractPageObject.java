package pageObjects;

import org.openqa.selenium.WebDriver;

public class AbstractPageObject {
	
	protected final WebDriver driver;
	
	public AbstractPageObject(WebDriver driver) {
		this.driver = driver;
	}
}
