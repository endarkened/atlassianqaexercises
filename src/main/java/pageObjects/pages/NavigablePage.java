package pageObjects.pages;

public interface NavigablePage {
	
	public void navigateByUrl();
	public void navigateByUrl(String url);
}
