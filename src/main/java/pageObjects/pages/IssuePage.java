package pageObjects.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import pageObjects.AbstractPageObject;

public class IssuePage extends AbstractPageObject {

	private static final By summaryLocator = By.id("summary-val");
	private static final By issueLinkLocator = By.className("issue-link");

	private String summary;
	private WebElement issueLink;

	public IssuePage(WebDriver driver) {
		super(driver);
		this.summary = this.driver.findElement(summaryLocator).getText();
		this.issueLink = this.driver.findElement(issueLinkLocator);
	}

	public String getSummary() {
		return this.summary;
	}

	public String getIssueName() {
		return this.issueLink.getText();
	}

}
