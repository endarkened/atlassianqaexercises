package pageObjects.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import pageObjects.AbstractPageObject;
import pageObjects.elements.CreateIssueSplash;

public class MainPage extends AbstractPageObject implements NavigablePage {
	
	public final static String URL = "https://jira.atlassian.com/browse/TST/";
	private static final By createIssueButton = By.id("create_link");
	
	public MainPage(WebDriver driver) {
		super(driver);
	}

	@Override
	public void navigateByUrl() {
		this.navigateByUrl(URL);
	}
	
	@Override
	public void navigateByUrl(String url) {
		this.driver.navigate().to(url);
	}
	
	public CreateIssueSplash createIssue() {
		this.driver.findElement(createIssueButton).click();
		return new CreateIssueSplash(this.driver);
	}
}
