package pageObjects.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import pageObjects.AbstractPageObject;

public class LoginPage extends AbstractPageObject implements NavigablePage {

	public LoginPage(WebDriver driver) {
		super(driver);
	}

	public final static String URL = "https://id.atlassian.com/login";

	private static final By usernameFieldLocator = By.name("username");
	private static final By passwordFieldLocator = By.name("password");
	private static final By signInButtonLocator = By.id("login-submit");

	public ProfilePage login(String username, String password) {
		
		this.driver.findElement(usernameFieldLocator).sendKeys(username);
		this.driver.findElement(passwordFieldLocator).sendKeys(password);
		this.driver.findElement(signInButtonLocator).click();
		
		return new ProfilePage(driver);
	}

	@Override
	public void navigateByUrl() {
		this.navigateByUrl(URL);
	}

	@Override
	public void navigateByUrl(String url) {
		this.driver.navigate().to(url);
	}

}
