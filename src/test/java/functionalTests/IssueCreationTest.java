package functionalTests;

import java.io.File;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.codeswarm.lipsum2.Lipsum;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import pageObjects.elements.CreateIssueSplash;
import pageObjects.elements.IssueCreatedSplash;
import pageObjects.pages.IssuePage;
import pageObjects.pages.LoginPage;
import pageObjects.pages.MainPage;
import utils.CredentialProvider;

public class IssueCreationTest {

	private final static String CHROME_DRIVER_PATH = "webdriver.chrome.driver";

	// if path to a driver not specified via system variable,
	// load proper executable from classpath depending on an OS
	static {
		if (System.getProperty(CHROME_DRIVER_PATH) == null) {
			System.setProperty(
					CHROME_DRIVER_PATH,
					ClassLoader
							.getSystemResource(
									"chromedriver"
											+ (System.getProperty("os.name")
													.toLowerCase()
													.contains("win") ? ".exe"
													: "")).getPath());

		}
	}

	private WebDriver driver;

	private final CredentialProvider cp;

	public IssueCreationTest() throws Exception {
		cp = new CredentialProvider();
	}

	@BeforeSuite
	public void setup() {
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10000, TimeUnit.MILLISECONDS);
	}

	private CreateIssueSplash createIssueSplash;
	private String summary;
	private String issueName;
	private IssuePage issuePage;

	@BeforeClass
	public void createAndOpen() {

		LoginPage lp = new LoginPage(driver);
		lp.navigateByUrl();
		lp.login(cp.getUsername(), cp.getPassword());
		MainPage mp = new MainPage(driver);
		mp.navigateByUrl();
		this.createIssueSplash = mp.createIssue();

		this.summary = Lipsum.lorem().paragraph(new Random().nextInt(150))
				.substring(0, 50).trim();

		this.createIssueSplash.enterSummary(summary);
		IssueCreatedSplash ics = this.createIssueSplash.submit();
		this.issueName = ics.getIssueName();
		System.out.println(this.issueName);
		this.issuePage = ics.navigateToIssue();
	}

	@Test
	public void testIssueHasTheSummary() {
		Assert.assertEquals(this.issuePage.getSummary(), summary);
	}

	@Test
	public void testIssueHasTheSameName() {
		Assert.assertEquals(this.issuePage.getIssueName(), this.issueName);
	}

	@AfterSuite(alwaysRun = true)
	public void tearDown() {
		this.driver.quit();
	}
}
